import axios from 'axios';

const EXERCISE_API_BASE_URL = "http://64.225.12.22:8888/api/v1/";

class ExerciseService {

    //Call API end point
    exerciseList() {
        console.log(EXERCISE_API_BASE_URL)
        return axios.get(EXERCISE_API_BASE_URL);
    }
    getDetailsByKey(key) {
        console.log(EXERCISE_API_BASE_URL + 'exercise/' + key + "/details")
        return axios.get(EXERCISE_API_BASE_URL + 'exercise/' + key + "/details");
    }
    getDetailsById(id) {
        console.log(EXERCISE_API_BASE_URL + 'exercise/' + id + "/details")
        return axios.get(EXERCISE_API_BASE_URL + 'exercise/' + id + "/details");
    }
    saveDetails(exercise_instance_id, data) {
        console.log(EXERCISE_API_BASE_URL + 'exercise/question/' + exercise_instance_id + "/submit")
        return axios.post(EXERCISE_API_BASE_URL + 'exercise/question/' + exercise_instance_id + "/submit", data);
    }
}

export default new ExerciseService()