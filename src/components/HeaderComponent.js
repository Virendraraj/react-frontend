import React, { Component } from 'react'

class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            students: [],
            exercises: []
        }
    }
    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                        <div className="collapse navbar-collapse" id="navbarsExampleDefault">
                            <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                Home <span className="sr-only">(current)</span>
                            </li>
                            </ul>
                        </div>
                    </nav>
                </header>
            </div>
        )
    }
}

export default HeaderComponent