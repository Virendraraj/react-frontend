import React, { Component } from 'react'
import ExerciseService from '../services/ExerciseService'
import * as Icon from 'react-bootstrap-icons';
import "../css/style.css";

var options = [];
const EXERCISE_API_BASE_URL = "http://64.225.12.22:3000";
var flag = false;
class ViewExerciseComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            exerciseDetails: [],
            results: [],
            merchandiseInventory: [],
            accountsPayable: [],
            accountspayableStatus : '',
            merchandiseinventoryStatus : '',
            transaction: ''
        }
    }
    handleValidation() {
        let formIsValid = true;
        let fields = this.state.exerciseDetails;
        /*fields.map((subitem, i) => {
            if (!subitem.userAnswer) {
                formIsValid = false;
                subitem.error = "Cannot be empty";
            }
        });*/
        this.setState({exerciseDetails: fields});
       return formIsValid;
    }
    handleBlur(field, e) {         
        let fields = this.state.exerciseDetails;
        // eslint-disable-next-line
        fields.map((subitem, i) => {
            if (field === i) {
                if (subitem.userAnswer && e.target.value && e.target.value.replace(/[^a-zA-Z0-9 ]/g, '').trim() !== subitem.answer.replace(/[^a-zA-Z0-9 ]/g, '').trim()) {
                    subitem.error = "Answer is wrong";
                    subitem.success = "";
                } else if (subitem.userAnswer && e.target.value && e.target.value.replace(/[^a-zA-Z0-9 ]/g, '').trim()  === subitem.answer.replace(/[^a-zA-Z0-9 ]/g, '').trim()) {
                    subitem.success = "Answer is correct";
                    subitem.error = "";
                }
            }
        });
        this.setState({exerciseDetails: fields});
    }
    
    exerciseSubmit(event) {
        event.preventDefault();
        var answer = true;
        if(flag == false) {
            answer = window.confirm("Are you sure you want to submit?");
        }
        if (answer) {  

            let excerciseList = [];
            let excercise = {};
            let exercise_instance_id = 0;
            let fields = this.state.exerciseDetails;
            let isTrue = flag;
            // eslint-disable-next-line
            fields.map((subitem, i) => {
                let excerciseObject = {
                    exercise_detail_id: Number(subitem.exercise_detail_id),
                    exercise_id: Number(subitem.exercise_id),
                    question_key : subitem.key,
                    username: 'test',
                    exercise_instance_id: Number(subitem.exercise_instance_id),
                    random_gen_answer_id:subitem.random_gen_answer_id,
                    isAnswerCheck : flag
                }
                exercise_instance_id = Number(subitem.exercise_id);
                excerciseObject.totalDebit = this.state.merchandiseInventory[this.state.merchandiseInventory.length - 1].debitsum;
                excerciseObject.totalCredit = this.state.accountsPayable[this.state.accountsPayable.length - 1].creditsum;
                excerciseObject.details = [];
                let finalMap = new Map();
                let word = "";
                for (var j = 0; j < (event.target.elements.length - 1); j++ ) {
                    if (event.target[j].offsetParent.offsetParent.offsetParent.id === subitem.key) {
                        // let detailsObject = {};
                        // detailsObject.answer_key = event.target[j].name;
                        // detailsObject.submitted_answer = event.target[j].value;
                        // excerciseObject.details.push(detailsObject);
                        //let n = Number(event.target[j].name.match(/\d+/g));
                        let text = event.target[j].name.match(/[a-zA-Z]+/g)[0];
                        
                        let textValue = event.target[j].value;

                            if (text == 'account' && event.target[j].value == '') {
                                word = '';
                            }
                            if (!finalMap.hasOwnProperty(textValue) && text == 'account' && event.target[j].value != '') {
                                word = textValue;
                                finalMap[textValue] = {};
                            } else if (text != 'account' && word != '') {
                                var finalMaps = finalMap[word];
                                console.log(word, text, event.target[j].value)
                                if (!finalMaps.hasOwnProperty(text)) {
                                    finalMaps[text] = event.target[j].value != '' ? Number(event.target[j].value) : '';
                                } else {
                                    finalMaps[text] = finalMaps[text] != '' && event.target[j].value != '' ? Number(finalMaps[text]) + Number(event.target[j].value) : '';
                                }
                                finalMap[word] = finalMaps;
                            } else {
                                word = textValue;
                            }
                        
                    }
                }
                Object.keys(finalMap).map((key, i) => {
                    let detailsObject = {};
                    detailsObject.answer_key = "account" + (i+1);
                    detailsObject.submitted_answer = key + "";
                    excerciseObject.details.push(detailsObject);
                    Object.keys(finalMap[key]).map((key1, j) => {
                        let detailsObject = {};
                        detailsObject.answer_key = key1 + (i+1);
                        detailsObject.submitted_answer = finalMap[key][key1] + "";
                        excerciseObject.details.push(detailsObject);
                    });
                });
                excerciseList.push(excerciseObject);
            });
           
            
            excercise.Exercise = excerciseList;
            ExerciseService.saveDetails(exercise_instance_id, excercise).then( res => {
                console.log(res.data)
                this.setState({results: res.data.result});
                this.setState({merchandiseinventoryStatus : res.data.merchandiseinventory});
                this.setState({accountspayableStatus : res.data.accountspayable});
                this.setState({journalEntry: res.data.journalEntry});
                console.log(this.state.accountspayableStatus, this.state.merchandiseinventoryStatus)
            })
        }
        /*if (this.handleValidation()) {
            console.log("success")
            let excerciseList = [];
            let excercise = {};
            let exercise_instance_id = 0;
            let fields = this.state.exerciseDetails;
            fields.map((subitem, i) => {
                let excerciseObject = {
                    username: 'test',
                    exercise_id: subitem.exercise_id,
                    exercise_instance_id: subitem.exercise_instance_id,
                    submitted_answer:  subitem.userAnswer,
                    question_key : subitem.key,
                    correct_answer: subitem.answer
                }
                exercise_instance_id = subitem.exercise_id;
                excerciseList.push(excerciseObject);
            });
            excercise.Exercise = excerciseList;
            ExerciseService.saveDetails(exercise_instance_id, excercise).then( res => {
                console.log(res.data)
                this.setState({results: res.data.result});
            })
        } else {
           return false;
        }*/
        return false;
    }

    componentDidMount() {
        ExerciseService.getDetailsById(this.state.id).then( res => {
            // eslint-disable-next-line
            res.data.data.map((subitem, i) => {
                subitem.userAnswer = "";
                subitem.error = "";
                subitem.success = "";
            });
            
            options = res.data.exerciseList.map(d => ({
                "value" : d.exercise_id,
                "label" : d.label
              }))
              
            this.setState({exerciseDetails: res.data.data});
            this.setState({exercises: options});
        });

        this.state.merchandiseInventory = [
            {debit:"",credit:"",debitsum:"",creditsum:""},
            {debit:"",credit:"",debitsum:"",creditsum:""},
            {debit:"",credit:"",debitsum:"",creditsum:""}
        ];
        this.state.accountsPayable = [
            {debit:"",credit:"",debitsum:"",creditsum:""},
            {debit:"",credit:"",debitsum:"",creditsum:""},
            {debit:"",credit:"",debitsum:"",creditsum:""}
        ];

    }

    handleChange(e) {
        let index = e.nativeEvent.target.selectedIndex;
        let label = e.nativeEvent.target[index].text;
        if (label && label.includes("Transaction")) {
            this.state.transaction = true;
        } else {
            this.state.transaction = false;
        }

        this.state.id = e.target.value;
        this.state.label = label;
        
        this.componentDidMount();
        window.history.pushState({}, null, EXERCISE_API_BASE_URL+"/exercises/"+this.state.id+"/details");
        this.state.results.length = 0;
    }
       
    getDetailsByKey(key) {
        this.props.history.push(`/view-exercise-details/${key}`);
    }
    confirm() {
        flag = false;
    }
    checkAnswer() {
        flag = true;
    }

    handleChangeM(i, event) {
        let fields = this.state.merchandiseInventory;
        fields[i][event.target.name] = event.target.value;
        this.setState({ fields });
     }
     handleChangeA(i, event) {
        let fields = this.state.accountsPayable;
        fields[i][event.target.name] = event.target.value;
        this.setState({ fields });
     }
    render() { 
        return (
            <div>
                 <br/>
                <div className = "card col-md-12">
                    <div class="select-exr">
                        <h5 class="exercise-label">Select Exercise  </h5> 
                        <select class="col-md-3 select-container" value={this.state.id} onChange={this.handleChange.bind(this)}>
                            {options.map((option) => (
                                <option value={option.value}>{option.label}</option>
                            ))}
                        </select>
                    </div>
                    <br/>
                    <h3 className = "text-center page-title">Exercise Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <form name="exerciseform" className="exerciseform" onSubmit= {this.exerciseSubmit.bind(this)} autocomplete="off">
                                <div class="column">
                                <table className = "table table-striped" border="1 ">
                                    <thead>
                                        <tr>
                                            <th class="detail_first_col"> Exercise Id</th>
                                            <th class="detail_third_col"> Detail Key</th>
                                            <th class="detail_fourth_col"> Question Text</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { 
                                            this.state.exerciseDetails && this.state.exerciseDetails.map((subitem, i) => {
                                                return (
                                                    <tr key = {subitem.key} id={subitem.key}> 
                                                        <td>{subitem.exercise_id}</td> 
                                                        <td> {subitem.key}</td>   
                                                        <td class = "sub_table" key = {subitem.key} id={subitem.key}>  <div key = {subitem.key} id={subitem.key} 
                                                                dangerouslySetInnerHTML={{
                                                                    __html: subitem.text 
                                                                }}></div> 
                                                            
                                                        </td>
                                                        
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <table className = "table table-striped fixed-layourt">    
                                        <tr>
                                            <td colSpan="4" class = "possibility_table">
                                                <table style={{float:"right",width:"22%"}} className="fixed-layourt">
                                                    <tr>
                                                        <th>
                                                            <span style={this.state.merchandiseinventoryStatus === true ? {color: "green", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Correct</span>
                                                            <span style={this.state.merchandiseinventoryStatus === false ? {color: "red", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Incorrect</span>
                                                     
                                                        </th>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <h6>
                                                    <div id="inventory">Merchandise Inventory</div>
                                                    </h6>
                                                </div>
                                                    
                                                    <table className="fixed-layourt">
                                                    <tbody>
                                                        <tr class="shadow_header">
                                                            <th>Date</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                        </tr>
                                                        { 
                                                            this.state.merchandiseInventory && this.state.merchandiseInventory.map((m, i) => {
                                                                return (
                                                                    <tr>
                                                                        <td>6/{i+1}</td>
                                                                        <td><input type="text" className="align-center" value={m.debit} name="debit" onChange={this.handleChangeM.bind(this, i)}></input></td>
                                                                        <td><input type="text" value={m.credit} name="credit" onChange={this.handleChangeM.bind(this, i)}></input></td>
                                                                        <td><input type="text" value={m.debitsum} name="debitsum" onChange={this.handleChangeM.bind(this, i)}></input></td>
                                                                        <td><input type="text" value={m.creditsum} name="creditsum" onChange={this.handleChangeM.bind(this, i)}></input></td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4" class = "possibility_table">
                                                <table style={{float:"right",width:"22%"}} className="fixed-layourt">
                                                    <tr>
                                                        <th>
                                                            <span style={this.state.accountspayableStatus === true ? {color: "green", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Correct</span>
                                                            <span style={this.state.accountspayableStatus === false ? {color: "red", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Incorrect</span>
                                                        </th>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <h6>
                                                        <div id="inventory">Accounts Payable</div>
                                                    </h6>
                                                </div>
                                                <table className="fixed-layourt">
                                                    <tbody>
                                                        <tr class="shadow_header">
                                                            <th>Date</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                        </tr>
                                                        { 
                                                            this.state.accountsPayable && this.state.accountsPayable.map((a, i) => {
                                                                return (
                                                                    <tr>
                                                                        <td>6/{i+1}</td>
                                                                        <td><input type="text" className="align-center" value={a.debit} name="debit" onChange={this.handleChangeA.bind(this, i)}></input></td>
                                                                        <td><input type="text" value={a.credit} name="credit" onChange={this.handleChangeA.bind(this, i)}></input></td>
                                                                        <td><input type="text" value={a.debitsum} name="debitsum" onChange={this.handleChangeA.bind(this, i)}></input></td>
                                                                        <td><input type="text" value={a.creditsum} name="creditsum" onChange={this.handleChangeA.bind(this, i)}></input></td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td colSpan="4" class="check_answer">
                                            <button id="submit" className="btn btn-primary" value="Submit" disabled={this.state.results.length > 0 && flag == false}  onClick={this.checkAnswer}>Check Answer</button>
                                            <button id="submit" className="btn btn-primary" style={{margin:"5px"}} value="Submit" disabled={this.state.results.length > 0 && flag == false} onClick={this.confirm}>Submit</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                               
                                <div className="text-center">
                                    
                                </div>
                            </form>
                            {flag == true &&
                                <table className = "table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th> Account</th>
                                                <th> Debit</th>
                                                <th> Credit</th>
                                                <th> Answer</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { 
                                                this.state.results.map((result, i) => {
                                                    return (
                                                        <tr key = {i}>
                                                            <td>
                                                                <div>
                                                                    {result.account}
                                                                </div>
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.debit}
                                                                </div>
                                                            
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.credit}
                                                                </div>
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    <span style={result.isaccountCorrect && result.isdebitCorrect && result.iscreditCorrect ? {color: "green", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Correct</span>
                                                                    <span style={!result.isaccountCorrect || !result.isdebitCorrect || !result.iscreditCorrect ? {color: "red", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Incorrect</span>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                }
                                            
                            {flag == false &&
                    
                            <div className = "card-body">
                                <div className = "row" style={this.state.results.length > 0 ? {display: 'block' } : {display: 'none'}}>
                                <table className = "table table-striped table-bordered finalResult">
                                        <thead>
                                        { 
                                            this.state.results.map((result, i) => {
                                                if(i==0){
                                                    return (
                                                        <tr>
                                                            <td colSpan="6">
                                                                <span style={result.result ? {color: "green", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Correct</span>
                                                                <span style={!result.result ? {color: "red", display: 'block', fontFamily: "fantasy"} : {display: 'none'}}>Incorrect</span>

                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            })
                                        }
                                        </thead>
                                    </table>
                                    <table className = "table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="12%">Exercise Instance Id</th>
                                                <th width="10%"> Detail Key</th>
                                                <th> Account</th>
                                                <th> Debit</th>
                                                <th> Credit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { 
                                                this.state.results.map((result, i) => {
                                                    return (
                                                        <tr key = {i}> 
                                                            <td>{result.exercise_instance_id}</td>
                                                            <td> {result.question_key}</td>
                                                            <td>
                                                                <div>
                                                                    {result.account}
                                                                    <span style={{float:"right"}}>
                                                                    <span style={result.isaccountCorrect ? {color: "green", display: 'block' } : {display: 'none'}}><Icon.CheckLg color="green" size={20} /></span>
                                                                    <span style={!result.isaccountCorrect ? {color: "red", display: 'block' } : {display: 'none'}}><Icon.XLg color="red" size={20} /></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.debit}
                                                                    <span style={{float:"right"}}>
                                                                    <span style={result.isdebitCorrect ? {color: "green", display: 'block' } : {display: 'none'}}><Icon.CheckLg color="green" size={20} /></span>
                                                                    <span style={!result.isdebitCorrect ? {color: "red", display: 'block' } : {display: 'none'}}><Icon.XLg color="red" size={20} /></span>
                                                                    </span>
                                                                </div>
                                                            
                                                            </td>
                                                            <td> 
                                                                <div>
                                                                    {result.credit}
                                                                    <span style={{float:"right"}}>
                                                                    <span style={result.iscreditCorrect ? {color: "green", display: 'block' } : {display: 'none'}}><Icon.CheckLg color="green" size={20} /></span>
                                                                    <span style={!result.iscreditCorrect ? {color: "red", display: 'block' } : {display: 'none'}}><Icon.XLg color="red" size={20} /></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                <br/>

                                <div className = "row" style={this.state.results.length > 0 ? {display: 'block' } : {display: 'none'}}>
                                    <table className = "grade_calculate">
                                        <thead>
                                            <tr>
                                                <th style={{"color":"red"}}>Scoring</th>
                                                <th> Total Possible</th>
                                                <th> Number Correct </th>
                                                <th> Grade </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <tr> 
                                                <td>Journal entries</td>
                                                <td>{this.state.journalEntry}</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td> Ledger accounts</td>
                                                <td> 0</td>
                                                <td> 0</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td> {this.state.results.length}</td>
                                                <td> 0</td>
                                                <td> 0 %</td>
                                            </tr>
                                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        }
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewExerciseComponent